﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Work_02.Models;

namespace Work_02.Controllers
{
    public class TraineesController : Controller
    {
        TraineeDbContext db = new TraineeDbContext();
        // GET: Trainees
        public ActionResult Index()
        {
            return View(db.Trainees.ToList());
        }

        // GET: Trainees/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Trainees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Trainees/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include ="TraineeId,TraineeName,Email,Course")]Trainee trainee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Trainees.Add(trainee);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(trainee);
               
            }
            catch
            {
                return View();
            }
        }

        // GET: Trainees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trainee trainee = db.Trainees.Find(id);
            if (trainee == null)
            {
                return HttpNotFound();
            }
            return View(trainee);
        }

        // POST: Trainees/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TraineeId,TraineeName,Email,Course")]Trainee trainee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(trainee).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(trainee);
                
            }
            catch
            {
                return View();
            }
        }

        // GET: Trainees/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Trainees/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
