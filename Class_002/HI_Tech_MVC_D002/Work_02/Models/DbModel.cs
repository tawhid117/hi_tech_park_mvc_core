﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Work_02.Models
{
    public enum Course
    {
        MVC, NT, JAVA, PHP
    }
    public class Trainee
    {
        public int TraineeId { get; set; }
        [Required,StringLength(50)]
        public string TraineeName { get; set; }
        [Required,StringLength(50),DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required,EnumDataType(typeof(Course))]
        public Course Course { get; set; }
    }
    public class TraineeDbContext : DbContext
    {
        public TraineeDbContext()
        {
            Database.SetInitializer(new DbInitializer());
        } 
        public DbSet<Trainee> Trainees { get; set; }
    }
    public class DbInitializer : DropCreateDatabaseIfModelChanges<TraineeDbContext>
    {
        protected override void Seed(TraineeDbContext context)
        {
            context.Trainees.Add(new Trainee { TraineeName = "Trainee 1", Email = "t1@gnmail.com", Course = Course.MVC });
            context.Trainees.Add(new Trainee { TraineeName = "Trainee 2", Email = "t2@gnmail.com", Course = Course.NT });
            context.Trainees.Add(new Trainee { TraineeName = "Trainee 3", Email = "t3@gnmail.com", Course = Course.JAVA });
            base.Seed(context);
        }
    }
}