﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Work_01.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [Required(ErrorMessage ="Product name is required!")]
        [StringLength(50)]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        [Required]
        [Display(Name = "Price/Item (TK.)")]
        [Column(TypeName ="money")]
        public decimal UnitPrice { get; set; }
        [Required]
        public bool Discontinued { get; set; }
    }
    public class ProductDbContext:DbContext
    {
        public DbSet<Product> Products { get; set; }
    }

}