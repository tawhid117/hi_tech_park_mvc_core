﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.IO;
using Work_01.Models;
using System.Collections.Generic;

namespace Work_01.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            string[] fruits = { "Apple", "banana", "Cherry", "Date", "Pineaple" };
            ViewBag.frs = fruits;

            ViewData["msg"] = "This is MVC Core Class";
            ViewData["fa"] = fruits;
            return View();
        }

        public IActionResult FileInformation()
        {
            string dr = Directory.GetCurrentDirectory();
            string[] files=Directory.GetFiles(@"D:\WorkFolder\gitLab\hi_tech_park_mvc_core\Class_001\");

            int i = 1;

            List<FileInformation> finList = new List<FileInformation>();

            foreach (var f in files)
            {
                FileInformation fin = new FileInformation();
                fin.Id = i;
                fin.Name = Path.GetFileNameWithoutExtension(f);
                fin.Extention = Path.GetExtension(f);
                fin.Path = f;
                fin.Size = new FileInfo(f).Length;

                finList.Add(fin);
                i++;
            }

            //ViewBag.res = finList;
            return View(finList);
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
