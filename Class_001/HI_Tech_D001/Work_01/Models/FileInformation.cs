﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Work_01.Models
{
    public class FileInformation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Extention { get; set; }
        public long Size { get; set; }
        public string Path { get; set; }
    }
}
